import React from 'react';
import { ListCardPage } from './ListPage'; 
import { TEST_QUERY } from '../queries';
import { MockedProvider } from '@apollo/client/testing'; // Use for Apollo Version 3+

export default {
  title: 'Blackbird/ListPage',
  component: ListCardPage,
};

// Mocked response from Graph QL
const mockGQLResponse = { test: [
  {id: 'test1', uuid: 'testUUID1', nickname: 'test 1'},
  {id: 'test2', uuid: 'testUUID2', nickname: 'test 2'},
  {id: 'test3', uuid: 'testUUID3', nickname: 'test 4'}
]};

// Mocked successful response
const successMocks = [
  {
    request: {
      query: TEST_QUERY,
    },
    result: {
      data: mockGQLResponse,
    },
  },
];

// Mocked 3s loading and then successful response
const loadingMocks = [
  {
    delay: 3000,
    request: {
      query: TEST_QUERY,
    },
    result: {
      data: mockGQLResponse,
    },
  },
];

// Mocked 1s loading and then failure response
const errorMocks = [
  {
    delay: 1000,
    request: {
      query: TEST_QUERY,
    },
    error: {
      status: 400,
      message: 'Failed to retrieve list'
    }
  },
];


const Template = () => <ListCardPage />;

// Note: apollo client mocks will fail snapshot testing because provider is mounted from the preview.js file
// This method, however, allows us to use storybook-addon-apollo-client
export const Success = Template.bind({});
Success.parameters= {
  storyshots: {disable: true},
  apolloClient: {
    mocks: successMocks
  },
};

export const Loading = Template.bind({});
Loading.parameters= {
  storyshots: {disable: true},
  apolloClient: {
    mocks: loadingMocks
  },
}

export const Error = Template.bind({});
Error.parameters= {
  storyshots: {disable: true},
  apolloClient: {
    mocks: errorMocks,
  },
}

// If we want to simulate snapshot testing with gql calls we can test this way
export const SuccessSnapshot = () => {
  const mocks = successMocks;
  return <MockedProvider mocks={mocks}>
    <ListCardPage />
  </MockedProvider>
}

export const LoadingSnapshot = () => {
  const mocks = loadingMocks;
  return <MockedProvider mocks={mocks}>
    <ListCardPage />
  </MockedProvider>
}

export const ErrorSnapshot = () => {
  const mocks = errorMocks;
  return <MockedProvider mocks={mocks}>
    <ListCardPage />
  </MockedProvider>
}



import { ListCard } from '../components/ListCard/ListCard';
import { TEST_QUERY } from '../queries';

import {
  useQuery,
} from "@apollo/client";

export const ListCardPage = () => {

  // Execute the graph ql request! 
  const { loading, error, data } = useQuery(TEST_QUERY);

  if(loading) {
    // If the request is loading, display this loading text
    return <p>loading!</p>;
  }

  if(error) {
    // If the request failed, log it to the console and display a generic error message
    console.info('ERROR', error);
    return <p>Error! </p>;
  } 

  // If the request succeeds, show the list card with the gql response
  return <ListCard
    listItems={data ? data.test : []}
  />
}
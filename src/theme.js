import { createTheme } from '@mui/material/styles';


export const blackbirdTheme = createTheme({
    palette: {
        primary: {
            main: '#0B3DF4',
        },
        formPrimary: {
            main: '#ffffff',
            contrastText: '#000000DE',
        },
        info: {
            main: '#0000008A',
        },
        border: {
            main: 'rgba(0, 0, 0, 0.12)'
        }
    },
});


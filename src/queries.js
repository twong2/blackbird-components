import { gql } from '@apollo/client';

// Test query for List Page
export const TEST_QUERY = gql`
query testQuery {
  test {
    id
    uuid
    nickname
  }
}`;
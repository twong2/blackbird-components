import { render, fireEvent, screen } from '@testing-library/react';
import { composeStory } from '@storybook/testing-react';
import { Empty as EmptyStory, Populated as PopulatedStory } from './ListCard.stories';


// unit tests

describe('List card tests', () => {

  test('SHOULD show empty message WHEN List card is empty', async () => {
    const Empty = composeStory(EmptyStory);
    render(<Empty  />);
    const emptyMessage = screen.queryByTestId('missing-list-items-message');
    expect(emptyMessage).toBeInTheDocument();
  });

  test('SHOULD show empty message WHEN search filter provides no results', async () => {
    const Populated = composeStory(PopulatedStory);
    render(<Populated  />);
    
    // before search input filters out all entries
    const nonExistentEmptyMessage = screen.queryByTestId('missing-list-items-message');
    expect(nonExistentEmptyMessage).not.toBeInTheDocument();
  
    const searchInput = screen.getByLabelText('Search');
    fireEvent.change(searchInput, {target: {value: 'some input text that doesnt exist asdasda'}})
  

    // after search input filters out all entries

    const emptyMessage = screen.queryByTestId('missing-list-items-message');

    expect(emptyMessage).toBeInTheDocument();
  });

  test('SHOULD empty out search text WHEN reset button is clicked', async () => {
    const Populated = composeStory(PopulatedStory);
    render(<Populated />);
    
    const searchInput = screen.getByLabelText('Search');
    fireEvent.change(searchInput, {target: {value: 'some input text that doesnt exist'}})
    
    const resetButton = screen.getByTestId('card-title__button');
    fireEvent.click(resetButton);

    const emptyMessage = screen.queryByTestId('missing-list-items-message');

    expect(emptyMessage).not.toBeInTheDocument();
    expect(searchInput.value).toBe('');
  });
})

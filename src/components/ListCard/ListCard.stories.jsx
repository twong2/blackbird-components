import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { ListCard } from './ListCard';
import { blackbirdTheme } from '../../theme';

export default {
  title: 'Blackbird/ListCard',
  component: ListCard,
};

const Template = (args) => {
    // apply blackbird theme
  return <ThemeProvider theme={blackbirdTheme}>
    <ListCard
      {...args}
    />
  </ThemeProvider>
};

export const Empty = Template.bind({});
Empty.args = {
    listItems: [],
};

export const Populated = Template.bind({});
Populated.args = {
    listItems: [
        {uuid: 'test-id-1', nickname: 'test nickname 1'},
        {uuid: 'test-id-2', nickname: 'test nickname 2'},
        {uuid: 'test-id-3', nickname: 'test nickname 3'},
        {uuid: 'test-id-4', nickname: 'test nickname 4'},
        {uuid: 'test-id-5', nickname: 'test nickname 5'}
    ],
};



import { useState } from 'react';
import PropTypes from 'prop-types';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import {CardTitle} from '../CardTitle/CardTitle';
import {ListRow} from '../ListRow/ListRow';
import {borderStyle} from '../../styleConstants';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Checkbox from '@mui/material/Checkbox';

/**
 * An example of a compound component
 * Notice how CardTitle is imported above and used within the ListCard component
 * 
 */

export const ListCard = (props) => {
    const {
        listItems
    } = props;

    const [searchValue, setSearchValue] = useState('');
    const [selectedItems, setSelectedItems] =  useState([]);

    const listItemsToShow = searchValue ? listItems.filter(li => li.nickname.includes(searchValue) || li.uuid.includes(searchValue)) : listItems;
    const totalListItemCount = listItems.length;

    const toggleItem = (uuid) => {
        const selectItemsSet = new Set(selectedItems);
        if(selectItemsSet.has(uuid)) {
            selectItemsSet.delete(uuid);
            
        } else {
            selectItemsSet.add(uuid);
        }
        setSelectedItems(Array.from(selectItemsSet));
    }
    const selectAllItems = () => setSelectedItems(listItemsToShow.map(li => li.uuid));
    const deselectAllItems = () => setSelectedItems([]);
    const isSelected = (item) => selectedItems.some(selecteduuid => item.uuid === selecteduuid);
    const reset = () => {
        deselectAllItems();
        setSearchValue('');
    }
    const selectedFilteredList = listItemsToShow.filter(li => selectedItems.includes(li.uuid));
    const numSelected = selectedFilteredList.length;

    return <TableContainer component={Paper} id='list-card__paper' elevation={1} sx={{ flexGrow: 1, overflow: 'hidden'}}>
        <CardTitle
            title='My Card Title!'
            searchValue={searchValue}
            border='none'
            setSearchValue={setSearchValue}
            buttonContents={<Button color='inherit' sx={{border: '1px solid #E0E0E0'}}>
            <Typography
                variant='p'
                noWrap
                component='div'
                >Reset</Typography>
            </Button>}
            buttonAction= {reset}
        />
        
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell padding="checkbox">
                        <Checkbox
                            color="primary"
                            indeterminate={numSelected > 0 && numSelected < totalListItemCount}
                            checked={totalListItemCount > 0 && numSelected === totalListItemCount}
                            onChange={() => numSelected ? deselectAllItems() : selectAllItems()}
                            inputProps={{
                                'aria-label': 'Select all items',
                            }}
                        />
                    </TableCell>
                    <TableCell>UUID</TableCell>
                    <TableCell>Nickname</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
            {listItemsToShow && listItemsToShow.length ?
                listItemsToShow.map((li, i) => 
                   <ListRow key={i} {...li} isSelected={isSelected(li)} toggleItem={toggleItem} />
                ) : <Box component={TableRow}
            id='missing-list-items-message'
            data-testid='missing-list-items-message'
            sx={{
                flexGrow: 1,
                borderTop: borderStyle,
                padding: '1rem'
            }}><TableCell
            
            colSpan={3}
            
            >No applicable list items</TableCell>
        </Box>}
            </TableBody>
        </Table>
        <Box
            sx={{
                flexGrow: 1,
                textAlign: 'right',
                padding: '1rem'
            }}><Typography
            variant='p'
            id='items-selected-message'
            data-testid='items-selected-message'
            >{numSelected} {numSelected === 1 ? 'item' : 'items'} selected</Typography>
        </Box>
       

    </TableContainer>
}


ListCard.propTypes = {
    // Complex object props
    listItems: PropTypes.arrayOf(PropTypes.shape({
        uuid: PropTypes.string,
        nickname: PropTypes.string,
    })),
};

CardTitle.defaultProps = {
    listItems: [],
};
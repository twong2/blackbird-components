
import PropTypes from 'prop-types';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import Checkbox from '@mui/material/Checkbox';

export const ListRow = (props) => {

    const {
        uuid,
        nickname,
        isSelected,
        toggleItem
    } = props;

    return  <TableRow>
        <TableCell padding="checkbox">
            <Checkbox
                color="primary"
                checked={isSelected}
                onChange={() => toggleItem(uuid)}
                inputProps={{
                    'aria-label': `Select uuid ${uuid}`,
                }}
            />
        </TableCell>
        <TableCell key='uuid'>{uuid}</TableCell>
        <TableCell key='nickname'>{nickname}</TableCell>
    </TableRow>
}

ListRow.propTypes = {
    uuid: PropTypes.string.isRequired,
    nickname: PropTypes.string.isRequired,
    isSelected: PropTypes.bool,
    toggleItem: PropTypes.func,
};
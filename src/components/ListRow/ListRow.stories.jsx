import React, { useState } from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { ListRow } from './ListRow';
import { blackbirdTheme } from '../../theme';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Blackbird/ListRow',
  component: ListRow,
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = (args) => {
    // apply blackbird theme
  const [isSelected, setIsSelected] = useState(args.isSelected);
  const toggleItem = () => setIsSelected(!isSelected);

  return <ThemeProvider theme={blackbirdTheme}>
    <ListRow
      {...args}
      isSelected={isSelected}
      toggleItem={toggleItem}
    />
  </ThemeProvider>
};


// Note these args only determine the state that the component starts with
// and doesnt control the way that the checkbox performs after toggling it
export const Unselected = Template.bind({});
Unselected.args = {
  uuid: 'unselected uuid test',
  nickname: 'unselected nickname',
  isSelected: false,
};

export const Selected = Template.bind({});
Selected.args = {
  uuid: 'Selected uuid test',
  nickname: 'Selected nickname',
  isSelected: true,
};




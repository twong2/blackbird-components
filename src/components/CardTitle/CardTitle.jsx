

import PropTypes from 'prop-types';
import Tooltip from '@mui/material/Tooltip';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import SearchIcon from '@mui/icons-material/Search';
import InfoIcon from '@mui/icons-material/Info';
import IconButton from '@mui/material/IconButton';
import Styles from './CardTitle.module.scss';
import {borderStyle} from '../../styleConstants';



export const CardTitle = (props) => {
    const {
        title,
        tooltipText,
        buttonContents,
        buttonAction,
        searchValue,
        setSearchValue,
        border,
    } = props;

    
    let boxBorderStyles = {};
    switch(border) {
        case 'all': boxBorderStyles = {border: borderStyle}; break;
        case 'bottom': boxBorderStyles = {borderBottom: borderStyle}; break;
        default: boxBorderStyles = {};
    }

    return <Box sx={{ flexGrow: 1, padding: '1.2rem 1rem', ...boxBorderStyles }}> 
        <Box sx={{display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <div className={Styles['titleContent__title']}>
                <Typography
                    variant='h5'
                    noWrap
                    component='div'
                >{title}</Typography>
                {tooltipText ? <Tooltip
                    id='card-title__tool-tip'
                    title={tooltipText} arrow>
                    <IconButton color='info'><InfoIcon fontSize='small'/></IconButton>
                </Tooltip> : null}
            </div>
            {
                buttonContents && buttonAction ? <span
                id='card-title__button'
                data-testid='card-title__button'
                onClick={buttonAction}>{buttonContents}</span> : null
            }
        </Box>
        {
            setSearchValue && searchValue !== null ? 
                <TextField
                id='card-title__search-input'
                data-testid='card-title__search-input'
                sx={{'marginTop': '1rem'}}
                value={searchValue} 
                fullWidth
                onChange={(e) => {
                    setSearchValue(e.target.value)
                }}
                InputProps={{
                    startAdornment: <InputAdornment position='start'><SearchIcon/></InputAdornment>
                }}
                label='Search'
            />: null
        }
    </Box>;
};

CardTitle.propTypes = {
    /**
     * Title of the card
     */
    title: PropTypes.string,
    /**
     * Tool tip text (Note: if not present, no tooltip icon is displayed)
     */
    tooltipText: PropTypes.string,

    
    /**
     * Right side content - this can be a button or icon etc.
     * Note: Without buttonContents and buttonAction, no right side button displays
     */
    buttonContents: PropTypes.node,
    /**
     * Right side content click action
     * Note: Without buttonContents and buttonAction, no right side button displays
     */
    buttonAction: PropTypes.func,

    /**
     * Current searched value
     */
    searchValue: PropTypes.string,
    /**
     * External function to set the current searched value in the parent state
     */
    setSearchValue: PropTypes.func,

    /**
     * The border setting of the component
     */
    border: PropTypes.oneOf(['all', 'bottom', 'none'])
};

CardTitle.defaultProps = {
    title: '',
    tooltipText: '',
    // right side button/icon
    buttonContents: null,

    // for the search bar
    searchValue: null,
    border: 'none',
};
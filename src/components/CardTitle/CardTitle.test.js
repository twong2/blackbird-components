import { render, fireEvent, screen } from '@testing-library/react';
import { composeStory } from '@storybook/testing-react';

import { FullyLoaded as FullyLoadedStory } from './CardTitle.stories'; 

describe('Card title tests', () => {

  test('Checks if the button was clicked', () => {
    // Note using compose story gives to access to more the underlying storybook configuration of the component
    // see more details at https://storybook.js.org/addons/@storybook/testing-react
    
    const FullyLoaded = composeStory(FullyLoadedStory);
    const testButtonFunc = jest.fn(); 
    render(<FullyLoaded buttonAction={testButtonFunc}  />);
      
    fireEvent.click(screen.getByTestId('card-title__button'));
  
    expect(testButtonFunc).toHaveBeenCalledTimes(1);
  });

  test('Checks if the button was clicked without using compose story', () => {
    const testButtonFunc = jest.fn(); 
    render(<FullyLoadedStory {...FullyLoadedStory.args} buttonAction={testButtonFunc}  />);
      
    fireEvent.click(screen.getByTestId('card-title__button'));
  
    expect(testButtonFunc).toHaveBeenCalledTimes(1);
  });
})

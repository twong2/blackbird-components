import React, {useState} from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { CardTitle } from './CardTitle';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { blackbirdTheme } from '../../theme';

export default {
  title: 'Blackbird/CardTitle',
  component: CardTitle,
  argTypes: {
    searchValue: {
      control: { disable: true }
    },
  },
};

const Template = (args) => {
  const [searchValue, setSearchValue] = useState(args.searchValue);
  
  // Add in theme context for MUI styling
  return <ThemeProvider theme={blackbirdTheme}>
    <CardTitle
      {...args}
      searchValue={searchValue}
      setSearchValue={args.setSearchValue ? (...params) => {
        args.setSearchValue(...params);
        setSearchValue(...params);
      }: null}
    />
  </ThemeProvider>
};

export const Basic = Template.bind({});
Basic.args = {
  border: 'all',
  title: 'Title of card',
};

export const BasicWithTooltip = Template.bind({});
BasicWithTooltip.args = {
  border: 'all',
  title: 'Title of card',
  tooltipText: 'Some tooltip text',
};

export const BasicWithAction = Template.bind({});
BasicWithAction.args = {
  border: 'all',
  title: 'Title of card',
  buttonContents: <IconButton color='primary'><AddIcon/></IconButton>,
  buttonAction: () => console.info('Content clicked'),
};

export const BasicWithButtonAction = Template.bind({});
BasicWithButtonAction.args = {
  border: 'all',
  title: 'Title of card',
  buttonContents: <Button color='inherit' sx={{border: '1px solid #E0E0E0'}}>
    <Typography
      variant='p'
      noWrap
      component='div'
    >Button</Typography>
  </Button>,
  buttonAction: () => console.info('Button clicked'),
};

export const BasicWithSearchBar = Template.bind({});
BasicWithSearchBar.args = {
  border: 'all',
  title: 'Title of card',
  searchValue: '',
  setSearchValue: () => console.info('Set search value'),
};


export const FullyLoaded = Template.bind({});
FullyLoaded.args = {
  border: 'all',
  title: 'Title of card',
  tooltipText: 'Some tooltip text',
  buttonContents: <IconButton color='primary' aria-label='some action'><AddIcon/></IconButton>,
  buttonAction: () => console.info('Plus clicked'),
  searchValue: '',
  setSearchValue: () => console.info('Set search value'),
};


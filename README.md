# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

This repo uses 
- Node Version: 16.13.1
- NPM Version: 8.1.2

Make sure to run `npm install` before attempting to run storybook! 

## Storybook

`npm run storybook`

Launches a local instance of storybook

Our primary use case for this repo

### Interaction testing

Start your storybook instance to view all the components and their corresponding stories

Plug and play different prop values to test out different scenarios! 
- Checkout how each component looks in different viewports (there is a viewport button in the top tool bar in the storybook interface)
- Experiment with wierd or unusual prop combinations - *Note: make sure to cross reference the true prop types in the corresponding -.stories.jsx file to make sure you aren't testing unrealistic scenarios*

### Unit testing

Storybook itself isn't used for unit tests (I've set up the unit tests using `@testing-library/react`). However, I am leveraging the storybook react testing node module to help port component stories into the unit tests.

This allows us to more cleanly execute tests on specific component scenarios rather than copy pasting props from the stories to the unit test.

The compose functions from the storybook react testing addon also provide additional properties to simplify the testing process (See `Reusing story properties` in the addon link)

Storybook react testing addon: https://storybook.js.org/addons/@storybook/testing-react/

### Accessibility testing

After starting your storybook instance, you should be able to view the accessibility criteria that a component has passed or violated. 

When looking at a story, you should see an "Accessibility" tab in the controls bar. Clicking into this should show you all of the accessibility tests ran on this component. 

The accessiblity criteria are based on the a11y project. On their website, they specify that they don't target all level A/AA accessibility concerns, however having a way to ensure that we are mimimizing violations is still valuable! 

See more details at A11y project website: https://www.a11yproject.com/checklist/

Storybook a11y addon: https://storybook.js.org/addons/@storybook/addon-a11y/

### Snapshot testing

This repo has snapshot testing already integrated! In the `src/__snapshots__` folder there should be the existing snapshots. 

This is intentionally NOT git ignored as everyone should have the same baseline snapshots

*Note: The automatic behaviour of snapshot testing is convenient but it is also the most error-prone. Snapshot tests and their statuses are meant only to be used in combination with the other testing methods.*

Storybook snapshot addon: https://storybook.js.org/addons/@storybook/addon-storyshots/

### GraphQL

See `src/components/pages/ListPage.jsx` and `src/components/pages/ListPage.stories.jsx` for examples on mocking graph ql requests

-----

## Available Scripts

In the project directory, you can run:

### `npm test` (the important one)

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

After launching the test runner you can: 
- press "w" to outline all of the testing options you have
- press "a" to run all tests
- press "f" to run only the failed tests
- etc. 

The tests should run on save

#### When a snapshot fails

Make sure to check the diff between the snapshot and your render. If the change is intended or inconsequential then feel free to press "u" to update the snapshots to align to your currently rendered components. 


### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
